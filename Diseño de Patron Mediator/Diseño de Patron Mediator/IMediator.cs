﻿using System;
using System.Collections.Generic;
using System.Net.Mail;
using System.Text;

namespace Diseño_de_Patron_Mediator
{
    public interface IMediator
    {
        void Send(string menssage, IColleague colleague);
    }
}
