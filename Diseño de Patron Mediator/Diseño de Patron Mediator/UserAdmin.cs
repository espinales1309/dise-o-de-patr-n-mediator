﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Diseño_de_Patron_Mediator
{


  public class  UserAdmin : IColleague
    {
        public UserAdmin(IMediator mediator ) : base (mediator )
        {

        }

        public override void Recelve(string message)
        {
            Console.WriteLine("Un administrador recibe: "+message);
            Console.WriteLine("Se notifico por hotmail "+ message );
        }
    }
}
