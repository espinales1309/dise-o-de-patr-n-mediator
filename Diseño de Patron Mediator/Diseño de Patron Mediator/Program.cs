﻿using System;

namespace Diseño_de_Patron_Mediator
{
    class D
    {
        static void Main(string[] args)
        {
            Mediator mediador = new Mediator();

            IColleague oPepe = new User(mediador);
            IColleague oAdmin = new  UserAdmin (mediador);
            IColleague oAdmin2= new  UserAdmin(mediador);


            mediador.Add(oPepe);
            mediador.Add(oAdmin);
            mediador.Add(oAdmin2);

            oPepe.communicate("oye admin tengo un problema");
        }
    }
}
