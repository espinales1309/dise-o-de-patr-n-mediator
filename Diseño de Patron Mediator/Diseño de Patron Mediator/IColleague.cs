﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Diseño_de_Patron_Mediator
{
    public abstract class IColleague
    {
        private IMediator mediator;

        public   IMediator Mediator
        {
            get;
        }

        public IColleague(IMediator mediator )
        {
            this.mediator = mediator;
        }
     

        public void communicate(string message)
        {
            this.mediator.Send(message, this);
        }

        public abstract void Recelve(string message); 
            
    }
}
